[Restic](https://github.com/restic/restic) backup service.

# Environment Variables
Take a look at the [restic manual](https://restic.readthedocs.io/en/stable/manual.html#initialize-a-repository)
to find relevant environment variabkes, depending on your backend.

## CRON_SCHEDULE_BACKUP
- default: "0 3 * * 1"

When to run the backup script.

## CRON_SCHEDULE_PRUNE
- default: "0 3 * * 3"

When to run the prune script.

## RESTIC_REPOSITORY
Path of the restic repository.

## RESTIC_SRC_DIR
The directory to backup.

## RESTIC_PASSWORD
Password for the restic repository.

## RESTIC_KEEP_HOURLY
- default: 1

Number of hourly archives to keep.

## RESTIC_KEEP_DAILY
- default: 4

Number of daily archives to keep.

## RESTIC_KEEP_WEEKLY
- default: 4

Number of weekly archives to keep.

## RESTIC_KEEP_MONTHLY
- default: 6

Number of monthly archives to keep.

## RESTIC_KEEP_YEARLY
- default: 2

Number of yearly archives to keep.
