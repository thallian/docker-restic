FROM golang:alpine as builder

ENV VERSION v0.9.0

RUN apk --no-cache add git libressl
RUN git clone https://github.com/restic/restic
RUN cd restic && git checkout ${VERSION}
RUN cd restic && go run build.go

FROM registry.gitlab.com/thallian/docker-confd-env:master

COPY --from=builder /go/restic/restic /bin/restic

RUN apk --no-cache add libressl ca-certificates

ADD /rootfs /
